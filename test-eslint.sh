#!/bin/bash

if npx eslint *.js >/dev/null
then touch .js-ok
else rm .js-ok 2>/dev/null
fi
