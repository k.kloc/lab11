import express from 'express';
import {body, validationResult} from 'express-validator';
import {Transaction} from "sequelize";

const {find_all_trips, find_trip} = await import('./db_files/queries.mjs');
const {database, Wycieczka, Zgloszenie} = await import('./db_files/database.mjs');
await import("./db_files/dummy.mjs");


const app = express();
app.use(express.urlencoded({extended: true}))

app.use((req, res, next) => {
    const date = new Date();
    const day = String(date.getDate()).padStart(2, '0');
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const year = String(date.getFullYear());

    console.log(`Żądanie nastąpiło w dniu ${day}/${month}/${year}`)

    if (day === "16" && month === '04') {
        res.locals.statement = `Przerwa w pracy serwera`;
        res.status(503).send(res.locals.statement);
        return
    }

    res.locals.statement = `Żądanie nastąpiło w dniu ${day}/${month}/${year}`;
    next();
})

app.get('/strona-testowa', (req, res) => {
    res.status(200).send('Strona Testowa - Hejka');
});

app.post('/strona-testowa', (req, res) => {
    res.status(501).send('Uzyj funkcji GET :)');
});

app.get('/wycieczka/:tripId(\\d+)', async (req, res) => {
    const trip = await find_trip(req.params.tripId);
    res.render('wycieczka', {trip});
});

app.route('/formularz/:tripId(\\d+)')
    .get(async (req, res) => {
        const trip = await Wycieczka.findByPk(req.params.tripId);
        res.render('formularz', {trip});
    })
    .post(
        body('first_name').notEmpty().withMessage("Imię nie może być puste!"),
        body('last_name').notEmpty().withMessage("Nazwisko nie może być puste!"),
        body('email').isEmail().withMessage("Proszę wpisać poprawny email!"),
        body('n_people')
            .isInt({min: 1})
            .withMessage("Liczba zgłoszeń musi być większa od 0!"),
        async (req, res) => {
            const t = await database.transaction({
                isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE,
            });
            const trip = await Wycieczka.findByPk(req.params.tripId, {
                transaction: t,
            });
            let errors = validationResult(req);
            if (!errors.isEmpty()) {
                const errors_to_show = errors.array().map(function (x) {
                    return x.msg;
                })
                await t.rollback();
                res.render("formularz", {trip, errors: errors_to_show})
                return
            }

            if (req.body.n_people > trip.liczba_dostepnych_miejsc) {
                await t.rollback();
                res.render("formularz", {trip, errors: ["Brak wystarczającej liczby wolnych miejsc!"]})
                return
            }

            try {
                await Zgloszenie.create({
                    imie: req.body.first_name,
                    nazwisko: req.body.last_name,
                    email: req.body.email,
                    liczba_miejsc: req.body.n_people,
                    WycieczkaId: req.params.tripId
                })
                trip.liczba_dostepnych_miejsc -= req.body.n_people;
                await trip.save({transaction: t});
                await t.commit();
                res.render('wycieczka', {trip, info: "Zarezerwowano wycieczkę"});

            } catch (err) {
                await t.rollback();
                res.render("formularz", {trip, errors: ["Niepoprawne dane!"]})
            }
        });

app.get('/strona-glowna', async (req, res) => {
    const trips = await find_all_trips();
    res.render('strona-glowna', {trips});
});

app.get('/data', (req, res) => {
    res.status(200).send(res.locals.statement);
})

app.get('/szablon', (req, res) => {
    res.render('mojszablon');
})

app.set('view engine', 'pug');
app.set('views', './views');
app.use(express.static('.', null));


app.listen(8080);
