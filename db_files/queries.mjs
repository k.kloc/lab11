const {Wycieczka} = await import('./database.mjs');
const { Op } = await import("sequelize");

async function find_all_trips() {
    return await Wycieczka.findAll({
        raw: true,
        where: {
            data_poczatku: {
                [Op.gt]: new Date(),
            }
        },
        order: [
            ['data_poczatku', 'ASC']
        ]
    })
}

async function find_trip(id) {
    return await Wycieczka.findByPk(id);
}

export {find_all_trips, find_trip}
