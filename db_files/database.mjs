import {Sequelize, DataTypes} from 'sequelize';

// Połączenie z bazą danych - lepiej użyć Postgresa (zadanie 0, patrz również niżej)
// Czy to może skończyć się błędem?
const database = new Sequelize('postgres://kk429317:iks@localhost:5432/bd');

// Zadanie 1
const Wycieczka = database.define('Wycieczka', {
    nazwa: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },
    opis: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    krotki_opis: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    obrazek: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cena: {
        type: DataTypes.FLOAT,
        allowNull: false
    },
    data_poczatku: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
            isLessOrEqualEndDate(value) {
                if (value > this.data_konca) {
                    throw new Error('Data początku powinna być mniejsza lub równa dacie końca wycieczki.');
                }
            }
        }
    },
    data_konca: {
        type: DataTypes.DATE,
        allowNull: false
    },
    liczba_dostepnych_miejsc: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
});

// Zadanie 2
const Zgloszenie = database.define('Zgloszenie', {
    imie: {
        type: DataTypes.STRING,
        allowNull: false
    },
    nazwisko: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            isEmail: true
        }
    },
    liczba_miejsc: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
});

// Zadanie 3
// Tu dodaj kod odpowiedzialny za utworzenie relacji pomiędzy modelami db.Wycieczka i db.Zgloszenie
Wycieczka.hasMany(Zgloszenie, {
    foreignKey: {
        allowNull: false
    }
})
Zgloszenie.belongsTo(Wycieczka)

// Zadania 4-6 w innych plikach

// ========================================
// Zadanie 0 - kontynuacja; proszę napisać kod poniżej, wg komentarzy

try {
    // Sprawdzenie poprawności połączenia (authenticate; co się dzieje, gdy błąd?)
    console.log('Nawiązuję połączenie z bazą...');
    await database.authenticate();
    console.log('Udało się.');

    // Jeśli modele zostały zmodyfikowane, to należy zmodyfikować tabele w bazie tak, by były zgodne.
    // Co się stanie z danymi? (sync)
    console.log('Synchronizuję modele z zawartością bazy...');
    await database.sync({force: true});
    console.log('Udało się.');
} catch (err) {
    // Nawiązywanie połączenia i synchronizacja mogły się nie udać, co wtedy?
    console.log("Nie udało się nawiązać połączenia");
    throw err;
}

export {database, Wycieczka, Zgloszenie};
