// Taka wersja import pozwoli obsłużyć ewentualne błędy...

const {Wycieczka, Zgloszenie} = await import('./database.mjs');

try {
    const morze = await Wycieczka.create({
        nazwa: "Dalekie morza",
        opis: 'Mórz jest wiele, wieś i opis może być nieco dłuższy niż poprzednio. Atrakcji tez może byé więcej.',
        krotki_opis: 'Wspaniała wycieczka nad morze.',
        obrazek: '../public/images/trips/morze.jpg',
        cena: 12344,
        data_poczatku: '2022-06-07 13:00:00',
        data_konca: '2022-06-14 13:00:00',
        liczba_dostepnych_miejsc: 0
    });

    const gory = await Wycieczka.create({
        nazwa: 'Szczyt wszystkiego',
        opis: 'Krótka wycieczka z wejściem na ten wlanie szczyt',
        krotki_opis: "Wspaniała wycieczka w góry.",
        obrazek: '../public/images/trips/gory.jpg',
        cena: 9876,
        data_poczatku: '2022-05-20 13:00:00',
        data_konca: '2022-05-27 13:00:00',
        liczba_dostepnych_miejsc: 40
    });

    const miasto = await Wycieczka.create({
        nazwa: 'Miasto',
        opis: 'Na świecie mamy jeszcze miasta, można je zwiedzać.',
        krotki_opis: "Wspaniała wycieczka do miasta.",
        obrazek: '../public/images/trips/miasto.jpg',
        cena: 54323,
        data_poczatku: '2022-07-07 13:00:00',
        data_konca: '2022-07-14 13:00:00',
        liczba_dostepnych_miejsc: 40
    });

    const zgloszenie1 = await Zgloszenie.create({
        imie: 'Jakub',
        nazwisko: 'Nowak',
        email: 'aaaa@gmail.com',
        liczba_miejsc: 3,
        WycieczkaId: gory.id
    })

    const zgloszenie2 = await Zgloszenie.create({
        imie: 'Jan',
        nazwisko: 'Kowalski',
        email: 'jkjk@gmail.com',
        liczba_miejsc: 1,
        WycieczkaId: gory.id
    })
} catch (err) {
    console.log("Nie udało sie dodać danych: ", err);
}
